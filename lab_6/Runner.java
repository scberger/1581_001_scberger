
public class Runner{
	public static void main(String[] args) {
		Fraction frac1 = new Fraction(1,2);
		Fraction frac2 = new Fraction(3,4);

		Fraction frac3 = frac1.add(frac2);
		System.out.println("The sum of "+frac1+" + "+frac2+" = "+frac3);

		Fraction frac4 = frac1.subtract(frac2);
		System.out.println("The diference of "+frac1+" - "+frac2+" = "+frac4);

		Fraction frac5 = frac1.multi(frac2);
		System.out.println("The product of "+frac1+" * "+frac2+" = "+frac5);

		Fraction frac6 = frac1.division(frac2);
		System.out.println("The quotient to "+frac1+" / "+frac2+" = "+frac6);
	}
}