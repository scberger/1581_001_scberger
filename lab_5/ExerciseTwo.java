//binary lookup table
//Author: Shayna Bergeron
//Date: 6/27/2018

import java.util.Scanner;

public class ExerciseTwo{
	public static void main(String[] args){
		String[] lookUpTable = {"0000", "0001", "0010", "0011",
								"0100", "0101", "0110", "0111", "1000"};

		Scanner input = new Scanner(System.in);

		int numberGiven = 0;

		while(numberGiven <= 8 && numberGiven >= 0){
			System.out.print("Enter a number from 0-8 to look up: ");
			numberGiven = input.nextInt();

			if(numberGiven <= 8 && numberGiven >= 0){
				String binaryVersion = lookUpTable[numberGiven];
				System.out.println("The numer " +numberGiven+ " is "
					+binaryVersion+ " in binary.\n");
			}
			else
				System.out.println(numberGiven+ " is not in the table.");
		}//end of while loop
	}//end of main
}//end of class