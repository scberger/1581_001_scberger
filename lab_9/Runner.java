//runner
//shayna bergeron
// 7/28/2018

public class Runner{
	public static void main(String[] args) {
		//points for triangle
		Point2D triVert1 = new Point2D(0.0, 0.0);
		Point2D triVert2 = new Point2D(0.0, 3.0);
  		Point2D triVert3 = new Point2D(4.0, 3.0);

  		Point2D circleCenter = new Point2D(5.5, 5.5);


  		Triangle myTriangle = new Triangle(triVert1, triVert2, triVert3);
  		Circle myCircle = new Circle(circleCenter, 2.0);

//  		System.out.println("This is the area of my triangle: "+myTriangle.area());
//  		System.out.println(myCircle+" has an area of "+myCircle.area());

  		Shape[] shapes = new Shape[2];
  		shapes[0] = myCircle;
  		shapes[1] = myTriangle;

  		for(Shape shape : shapes){
  			System.out.println(shape+ " has area: " + shape.area()+"\n");
  		}

  		for(Shape shape : shapes){
  			shape.moveVertical(2.0);
  			shape.moveHorizontal(-3.0);
  		}

  		System.out.println("After the move\n");
  		for(Shape shape : shapes){
  			System.out.println(shape+ " has area: " + shape.area()+"\n");
  		}

	}
}