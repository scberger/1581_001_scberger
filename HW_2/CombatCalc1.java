// Iteration 1/7 - Declaring monster and hero variables

//Author: Shayna Bergeron
//Date: 6/13/2018

public class CombatCalc1{
	public static void main(String[] args) {
		/* Monster data variables */
		String goblin;
		goblin = "Goblin"; //Initializing the monster's name
		int monsterHealth;
		monsterHealth = 100; //Initializing the monster's health
		int monsterAttackPower;
		monsterAttackPower = 15; //Initializing the monster's attack power

		/*Hero data variables*/
		int heroHealth;
		heroHealth = 100; //Initializing the hero's health
		int heroAttackPower;
		heroAttackPower = 12; //Initializing the hero's attack power
		int heroMagicPower;
		heroMagicPower = 0; //Initializing the hero's magic power

	}
}