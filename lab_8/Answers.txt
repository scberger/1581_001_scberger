Lab 8 Answers
Author: Shayna Bergeron
Date: 7/11/2018

1. The equals method is comparing the memory location.
2. The points could have the same coordinates, so therefore we would think it is the same.
3. The toString is returning the memory location.
4. We would think it would print the x, y coordinates.
5. The string output is the coordinates of the points, it is the same as before. It overidden the 
original so it no longer has access to it.
6. The superclass is at the top of the hierarchy, so we can reference the super since the subclasses
inherit from the super.
7. Yes, subclasses have access to parent classes add methods. We don't need to reimplement things.
8. Yes, the program created a SetOfPoints that was a Triangle, it successfully complied and ran.
A triangle is a SetOfPoints.
9. Casting set2 as set2 did not work, bust casting it as tri worked because Triangle has an
are method.
10. A Line segment is a set of points but a set of points is not a line segment.