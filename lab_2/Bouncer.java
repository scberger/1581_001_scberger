/** This is a bouncer program that checks the age of people 
*
*Author: Shayna Bergeron
*Version: 6/11/2018
*/
import java.util.Scanner;

public class Bouncer{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int age;
		int lineCount;
		System.out.println("How many people are in line?");
		lineCount = input.nextInt();

		while(lineCount > 0){
			System.out.println("I need to see some ID, please.");
			age = input.nextInt();

			if(age < 18){
				System.out.println("Sorry kid, you're not old enough.");
			}

			else if(age >= 18 && age < 21){
				System.out.println("Come on in, but NO DRINKS!");
			}

			else {
				System.out.println("Help yourself to booze!");
			}

			System.out.println("Nonetheless, have a good night!");
			
			lineCount--;

		} //end while

		System.out.println("Well, I guess I'll go take a nap.");
	} // end main
} // end Bouncer