//Sizable array lab
//Author: Shayna Bergeron
//Date: 7/9/2018

import java.util.Arrays;

public class DynamicArray{
	private String[] array;
	private int nextIndex;
	private int numberOfElements;

	//constructor
	public DynamicArray(){
		this.array = new String[10];
		this.nextIndex  = 0;
		this.numberOfElements = 0;
	}//end of no argument constructor

	public DynamicArray(int size){
		this.array = new String[size];
		this.nextIndex = 0;
		this.numberOfElements = 0;
	}//end of one argument contructor

	public DynamicArray(String[] initialStrings){
		this.array = Arrays.copyOf(initialStrings, initialStrings.length);
		this.nextIndex = initialStrings.length;
		this.numberOfElements = initialStrings.length;
	}//end of overloaded one-argument constructor

	public void add(String stringToAdd){
		if(this.numberOfElements == this.array.length){
			this.resizeArray();
		}

		array[nextIndex] = stringToAdd;
		nextIndex++;
		numberOfElements++;

	}//end of add method

	private void resizeArray(){
		String [] newArray = new String[array.length+array.length/2];
		for(int i=0; i<array.length; i++){
			newArray[i] = array[i];
		}//end of for loop
		array = newArray;
	}//end of resize array ()

	public String remove(int indexToRemove){
		if(indexToRemove<nextIndex){
		String removedString = this.array[indexToRemove];

		for(int i = indexToRemove; i<nextIndex-1; i++){
			this.array[i] = this.array[i+1];
		}
		numberOfElements--;
		nextIndex--;
		return removedString;
		}
		else
			return "That is not a valid index to remove!!!";
	}//end of remove()

	public String get(int index){
		if(index<numberOfElements){
		return this.array[index];
		}
		else
			return "That is not a valid index!!!";
	}//end of get()

	public boolean isEmpty(){
		if(numberOfElements ==0){
			return true;
		}
		else 
			return false;
	}//end of isEmpty()

	public int sizeOf(){
		return this.numberOfElements;
	}//end of size ()

}//end of class