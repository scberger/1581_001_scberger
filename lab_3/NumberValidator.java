//Asks user for specific numbers and validates if they are right

//Author: Shayna Bergeron
//Version: 6/13/2018

import java.util.Scanner;

public class NumberValidator{
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int num;

		System.out.print("Enter a number between 0 and 10: ");
		num = input.nextInt();
		if(num > 0 && num < 10){
			System.out.println("Valid Input\n");
		}
		else
			System.out.println("Invalid Input\n");

		System.out.print("Enter a number that is divisible by 2 or 3: ");
		num = input.nextInt();
		if(num % 2 == 0 || num % 3 == 0){
			System.out.println("Valid Input\n");
		}
		else
			System.out.println("Invalid Input\n");

		System.out.print("Enter a number that is negative and even or positive and odd: ");
		num = input.nextInt();
		if((num < 0 && num % 2 ==0) || (num > 0 && num % 2 == 1)){
			System.out.println("Valid Input\n");
		}
		else
			System.out.println("Invalid Input\n");

		System.out.print("Enter a number divisible by 2 or 5, but not both: ");
		num = input.nextInt();
		if(((num%2==0) || (num%5==0)) && !(num%10==0)){
			System.out.println("Valid Input\n");
		}
		else
			System.out.println("Invalid Input\n");

		System.out.print("Enter a number that is odd and positive: ");
		num = input.nextInt();
		if(num%2==1 && num>0){
			System.out.println("Valid Input\n");
		}
		else
			System.out.println("Invalid Input\n");
	
	}// end main
}// end class