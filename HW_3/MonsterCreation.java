// Refinement 2/3 - Creating class variables and methods for monster stats
//Author: Shayna Bergeron 
//Date: 6/19/2018

import java.util.Scanner;
import java.util.Random;

public class MonsterCreation{
	private static int heroHealth = 0;
	private static int heroAttack = 0;
	private static int heroMagicPower = 0;
	private static int statVal = 0;
	private static int monsterHealth = 0;
	private static int monsterAttack = 0;
	private static int xp = 0;
	private static String monsterName = "";
	static Scanner input = new Scanner(System.in);

	/*start of main*/
	public static void main (String[] args){
		heroStats();
		monsterStats();
	}// end of main
	/*start of heroStats method*/
	public static void heroStats(){
		int playerChoice;
		for(statVal = 20; statVal > 0; statVal--){
			/*hero stats menu*/
			System.out.printf("%s%s%s%n", "Health:" + heroHealth, " Attack:" +heroAttack, " Magic:" +heroMagicPower);
			System.out.println("1.) +10 Health");
			System.out.println("2.) +1 Attack");
			System.out.println("3.) +3 Magic");
			System.out.printf("%n");
			System.out.print("You have " +statVal+ " points to spend: ");
			playerChoice = input.nextInt();
			System.out.printf("%n");

			/*if else statement to run options*/
			if(playerChoice == 1){
				heroHealth = heroHealth + 10;
			}
			else if(playerChoice == 2){
				heroAttack++;
			}
			else if(playerChoice == 3){
				heroMagicPower = heroMagicPower + 3;
			}
			else{
				statVal++;
				System.out.println("That is not a valid choice");
				System.out.printf("%n");
			}			
		}//end of for loop for heroStats
	}//end of heroStats method
	/*declaring monster stat method*/
	public static void monsterStats(){
		/*random num generator for monster*/
		Random number = new Random();
		
		int monsterPick = number.nextInt(3);
		if(monsterPick == 0){
			monsterName = "Goblin";
			monsterAttack = 8 + number.nextInt(5);
			monsterHealth = 75 + number.nextInt(25);
			xp = 1;
		}
		else if(monsterPick == 1){
			monsterName = "Orc";
			monsterAttack = 12 + number.nextInt(5);
			monsterHealth = 100 + number.nextInt(25);
			xp = 3;
		}
		else if(monsterPick == 2){
			monsterName = "Troll";
			monsterAttack = 15 + number.nextInt(5);
			monsterHealth = 150 + number.nextInt(50);
			xp = 5;
		}
		System.out.println("You have encountered " +monsterName+".");
		System.out.printf("%n");
	}// end of monsterStats method
}// end of class  