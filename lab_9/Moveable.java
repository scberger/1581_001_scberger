//shayna bergeron
// 7/23/2018

public interface Moveable{
	void moveVertical(double distance);

	void moveHorizontal(double distance);
}