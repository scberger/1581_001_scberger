// Iteration 2/7 - Printing monster and player stats

//Author: Shayna Bergeron
//Date: 6/13/2018

public class CombatCalc2{
	public static void main(String[] args) {
		/* Monster data variables */
		String goblin;
		goblin = "Goblin"; //Initializing the monster's name
		int monsterHealth;
		monsterHealth = 100; //Initializing the monster's health
		int monsterAttackPower;
		monsterAttackPower = 15; //Initializing the monster's attack power

		/*Hero data variables*/
		int heroHealth;
		heroHealth = 100; //Initializing the hero's health
		int heroAttackPower;
		heroAttackPower = 12; //Initializing the hero's attack power
		int heroMagicPower;
		heroMagicPower = 0; //Initializing the hero's magic power

		/*Prints out stats of player and monster*/
		System.out.println("You are fighting a " +goblin+ "!"); 
		System.out.println("The monster's HP: " +monsterHealth);
		System.out.println("Your HP: " +heroHealth);
		System.out.println("Your MP: " +heroMagicPower);
	}
}