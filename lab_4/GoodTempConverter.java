import java.util.Scanner; 

/**
 *A changed version of BadTempConverter to use methods 
 *
 * Remember: 
 * 		tempCelsius = (tempFahr - 32) / 1.8 
 * 		tempFahr = tempCelsius * 1.8 + 32
 *
 * Author: Shayna Bergeron
 * Date: 6/18/2018
 */ 
public class GoodTempConverter {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); 
		int userChoice = 0; 
		double tempCelsius=0; 
		double tempFahr=0; 
		
		// Loop until user opts to quit program. 
		while (userChoice != 3) {
			printMenu();
			
			// Get user's choice. 
			userChoice = input.nextInt(); 
			runChoice(userChoice, tempFahr, tempCelsius, input);
			
		} // end main program loop
	} // end method main

	private static void printMenu(){
		// Print the user's options. 
			System.out.println("Please select one of the following options: "); 
			System.out.println("	1. Convert Fahrenheit to Celsius; "); 
			System.out.println("	2. Convert Celsius to Fahrenheit; or "); 
			System.out.println("	3. Quit "); 
	}// end of printMenu

	private static void fahrToCel(double tempFahr){
		double tempCelsius = (tempFahr - 32) / 1.8; 
		System.out.printf("%.2f degrees Fahrenheit is equal to %.2f degrees Celsius.%n",
			tempFahr, tempCelsius); 
	}
	private static double celToFahr(double tempCelsius){
		return(tempCelsius * 1.8 + 32);

	}
	private static void runChoice(int userChoice, double tempFahr, double tempCelsius, Scanner input){
					if (userChoice == 1) {
				System.out.printf("Please enter the temperature in Fahrenheit: "); 
				tempFahr = input.nextDouble(); 
				fahrToCel(tempFahr);
			} 
			
			// User wants to convert Celsius to Fahrenheit. 
			else if (userChoice == 2) {
				System.out.printf("Please enter the temperature in Celsius: "); 
				tempCelsius = input.nextDouble(); 
				tempFahr = celToFahr(tempCelsius); 
				System.out.printf("%.2f degrees Celsius is equal to %.2f degrees Fahrenheit.%n", 
					tempCelsius, tempFahr); 
			} 
			
			// User wants to quit the program. 
			else if (userChoice == 3) {
				System.out.println("Thanks! Have a nice day!");  
			} 
			
			// User entered invalid input. 
			else {
				System.out.println("Invalid input. Please enter only 1, 2, or 3."); 
			} 
	}

} // end class BadTempConverter