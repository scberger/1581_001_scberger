// collects month from user and checks if it is valid

//Author: Shayna Bergeron
//Version: 6/13/2018

import java.util.Scanner;

public class MonthGetter{
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int month;

		do{
			System.out.print("Enter a month between 1 - 12: ");
			month = input.nextInt();
		} while(!(month >= 1 && month <= 12));

		System.out.println("Month " + month+ " is a valid month!");
	}
}