// Refinement 1/3 - Creating class variables and methods for hero stats
//Author: Shayna Bergeron 
//Date: 6/19/2018

import java.util.Scanner;

public class HeroCreation{
	private static int heroHealth = 0;
	private static int heroAttack = 0;
	private static int heroMagicPower = 0;
	private static int statVal = 0;
	static Scanner input = new Scanner(System.in);

	/*start of main*/
	public static void main (String[] args){
		heroStats();
	}// end of main
	/*start of heroStats method*/
	public static void heroStats(){
		int playerChoice;
		for(statVal = 20; statVal > 0; statVal--){
			/*hero stats menu*/
			System.out.printf("%s%s%s%n", "Health:" + heroHealth, " Attack:" +heroAttack, " Magic:" +heroMagicPower);
			System.out.println("1.) +10 Health");
			System.out.println("2.) +1 Attack");
			System.out.println("3.) +3 Magic");
			System.out.printf("%n");
			System.out.print("You have " +statVal+ " points to spend: ");
			playerChoice = input.nextInt();
			System.out.printf("%n");

			/*if else statement to run options*/
			if(playerChoice == 1){
				heroHealth = heroHealth + 10;
			}
			else if(playerChoice == 2){
				heroAttack++;
			}
			else if(playerChoice == 3){
				heroMagicPower = heroMagicPower + 3;
			}
			else{
				statVal++;
				System.out.println("That is not a valid choice");
				System.out.printf("%n");
			}			
		}//end of for loop for heroStats
	}//end of heroStats method
}// end of class  